import './string.js'


class WeatherView {
    constructor() {
        this.location = document.getElementById('location');
        this.description = document.getElementById('description');
        this.temperature = document.getElementById('temperature');
        this.icon = document.getElementById('icon');
        this.wind = document.getElementById('wind');
        this.pressure = document.getElementById('pressure');
        this.humidity = document.getElementById('humidity');
    }

    update(data) {
        this.location.textContent = `${data.name}, ${data.sys.country.toUpperCase()}`;
        this.description.textContent = data.weather[0].description.capitalize();
        this.temperature.textContent = `${data.main.temp} °C`;
        this.icon.src = `https://openweathermap.org/img/w/${data.weather[0].icon}.png`;
        this.wind.textContent = `Wind: ${data.wind.speed} m/s, ${data.wind.deg}°`;
        this.pressure.textContent = `Pressure: ${data.main.pressure} hPa`;
        this.humidity.textContent = `Humidity: ${data.main.humidity}%`;
    }
}


export default WeatherView;
