export default class OpenWeatherMapAPIClient {
    constructor(apiKey) {
        this.apiKey = apiKey;
        this.units = 'metric';
    }

    async getWeather(city, country) {
        let queryUrl = `https://api.openweathermap.org/data/2.5/weather?q=${city}`;
        if (country) queryUrl += `,${country}`;
        if (this.units) queryUrl += `&units=${this.units}`;
        queryUrl += `&appid=${this.apiKey}`;

        const response = await fetch(queryUrl);

        return await response.json();
    }
};
