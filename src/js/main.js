import config from './config.js/index.js.js';
import OpenWeatherMapAPIClient from './open-weather-map-api-client.js';
import WeatherView from './weather-view.js';


const weatherView = new WeatherView();

const openWeatherMapAPIClient = new OpenWeatherMapAPIClient(config.OPEN_WEATHER_MAP_API_KEY);
openWeatherMapAPIClient.getWeather('Novokuznetsk')
    .then((data) => {
        weatherView.update(data);
        console.log(data);
    });
